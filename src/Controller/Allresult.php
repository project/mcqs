<?php

namespace Drupal\quizers\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;


/**
 * Provides route responses for the Example module.
 */
class Allresult extends ControllerBase {

  /**
   * Adding variable.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Adding variable.
   *
   * @var Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Adding variable.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Adding variable.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messanger;


  /**
   * Implements cunstructor.
   */
  public function __construct(AccountInterface $account, Connection $database, RequestStack $request, MessengerInterface $messanger) {
    $this->account = $account;
    $this->database = $database;
    $this->request = $request;
    $this->messanger = $messanger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user'),
    $container->get('database'),
    $container->get('request_stack'),
    $container->get('messenger')
    );
  }

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function myPage() {
    $query = $this->database->select('usersquiz_data', 'qt');
    $query->fields('qt', [
      'id', 'subscriber', 'quiz_name', 'duration', 'question_count', 'result',
    ]);
    $data_result = $query->execute();
    $header = [
      'userid' => t('User id'),
      'subscriber' => t('Subscriber'),
      'quiz_name' => t('Quiz name'),
      'druation' => t('Duration'),
      'question_count' => t('Points'),
      'result' => t('Result'),
      
    ];
    foreach($data_result as $tbl) {
      $rows[] = [
        'userid' =>$tbl->id,
        'subscriber' =>$tbl->subscriber,
        'quiz_name' =>$tbl->quiz_name,
        'duration' =>$tbl->duration,
        'question_count' =>$tbl->question_count,
        'result' =>$tbl->result,

      ];
    }
    
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No users found'),
    ];
    return $form;
  }

}